#!/usr/bin/env python
import roslib; roslib.load_manifest('mikrokopter')
import rospy
from mikrokopter.msg import Control
import numpy

class MonitorObject(object):
	def __init__(self):
		self.activeFrames = numpy.zeros(255, dtype=numpy.int)
		rospy.Subscriber("/mikrokopter/req_set_control", Control, self.externctrl_req_callback)
		rospy.Subscriber("/mikrokopter/set_control_ack", Control, self.externctrl_ack_callback)
		rospy.Timer(rospy.Duration(0.1), self.wdTimer)

	def externctrl_req_callback(self,req):
		self.activeFrames[req.frame] = 1
		self.req_active = True

	def externctrl_ack_callback(self,ack):
		self.activeFrames[ack.frame] = 0
		self.ack_active = True

	def wdTimer(event):
		if self.req_active:
			if not self.ack_active:
				rospy.logerr('Mikrokopter not acknowledging external control requests!')
		num_unanswer_reqs = self.activeFrames.count(1)
		rospy.loginfo('Unanswered external ctrl reqs: %s', num_unanswer_reqs)
		self.req_active = False
		self.ack_active = False
      
def listener():
	rospy.init_node('externCtrlMonitor')
	mo = MonitorObject()
	rospy.spin()

if __name__ == '__main__':
	listener()
