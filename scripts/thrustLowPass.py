#!/usr/bin/env python
import roslib; roslib.load_manifest('mikrokopter')
import rospy
from math import exp,log
from mikrokopter.msg import Control
import sys



class thrustLowPass(object):
    def __init__(self):
        self.oldThrust = 160.0
        rospy.Subscriber('/mikrokopter/req_set_control', Control, self.getCommand)
    def getCommand(self,data):
        self.oldThrust = 0.995 * self.oldThrust + 0.005 * data.thrust
        rospy.loginfo("Thrust average %f",self.oldThrust)
        
if __name__ == '__main__':
    rospy.init_node('thrustlowpass')
    j = thrustLowPass()
    rospy.spin()
