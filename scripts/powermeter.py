#!/usr/bin/env python
import roslib; roslib.load_manifest('mikrokopter')
import rospy
from mikrokopter.msg import PowerMeter
from mikrokopter.msg import FcDebug
from std_msgs.msg import Empty

'''Converts a PPM signal to the mikrokopter status'''

class PowerMeterObject(object):
	def __init__(self):
		self.prev_wattsUsed = 0
		self.previous_time = rospy.Time.now()
		self.previous_voltage = 0
		self.battCapacity_Wh = rospy.get_param('battCapacity_Wh', 130)
		self.payloadPower_W = rospy.get_param('payloadPower_W', 45)
		self.alpha = rospy.get_param('lp_alpha', 0.05)
		self.flightPower_W = rospy.get_param('flightPower_W', 800)
		self.avgWatts = self.flightPower_W

		self.pub = rospy.Publisher('/mikrokopter/powermeter', PowerMeter)
		rospy.Subscriber("/mikrokopter/fc_debug", FcDebug, self.fcdebug_callback)
		rospy.Subscriber("/mikrokopter/powermeter/reset", Empty, self.reset_callback)

	def reset_callback(self,reset):
		self.prev_wattsUsed = 0

	def fcdebug_callback(self,fcdebug):
		msg              = PowerMeter()
		msg.header       = fcdebug.header
		
		msg.current_A = (fcdebug.current/10.0)
		msg.voltage_V = (fcdebug.batteryVoltage/10.0)
		msg.power_W = msg.voltage_V * msg.current_A + self.payloadPower_W
        
		if self.previous_time.to_sec() < 1:
			self.previous_time = fcdebug.header.stamp

		dt = fcdebug.header.stamp.to_sec() - self.previous_time.to_sec()
		dV = msg.voltage_V - self.previous_voltage

		#Check if a new set of batteries have been installed
		# If more than 60 seconds have past and the battery voltage has increased by 1.5V, reset the meter
		if (dt > 60 and dV > 1.5):
			self.prev_wattsUsed = 0
			
		wattHours = msg.power_W * dt / 3600.0
		self.avgWatts = self.alpha*msg.power_W + (1-self.alpha)*self.avgWatts

		msg.used_Wh    = self.prev_wattsUsed + wattHours

		msg.battCapacity_Wh = self.battCapacity_Wh
		msg.remaining_Wh = msg.battCapacity_Wh - msg.used_Wh

		msg.timeRemaining_min = (msg.remaining_Wh / self.avgWatts) * 60
		msg.flightTimeRemaining_min = (msg.remaining_Wh / self.flightPower_W) * 60

		self.pub.publish(msg)

		self.prev_wattsUsed = msg.used_Wh
		self.previous_time = fcdebug.header.stamp
		self.previous_voltage = msg.voltage_V

        
def listener():
	rospy.init_node('power_meter')
	pm = PowerMeterObject()
	rospy.spin()

if __name__ == '__main__':
	listener()
