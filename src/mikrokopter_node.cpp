 /*!
  *  \file    mikrokopter_node.cpp
  *  \brief   Instantiates the MikrokopterInterface class
  *  \author  Andrew Chambers <achamber@andrew.cmu.edu>
  *  \author  Justin Haines   <jhaines@andrew.cmu.edu>
  *  \author  Sebastian Scherer <basti@andrew.cmu.edu>
  *  \date    2011 October 25
  *  \details This program is a ROS wrapper for the MikrokopterInterface
  */

#include <ros/ros.h>
#include <mikrokopter/interface.h>

using namespace CA;

int main(int argc, char **argv)
{
   ros::init(argc, argv, "mikrokopter");
   ros::NodeHandle n;

   ros::NodeHandle np("~");

   MikrokopterInterface interface(n);
   ros::spin();
}
