 /*!
  *  \file    externctrl_client.cpp
  *  \brief   A demonstration of the MikrokopterInterface class
  *  \author  Andrew Chambers <achamber@andrew.cmu.edu>
  *  \author  Justin Haines   <jhaines@andrew.cmu.edu>
  *  \author  Sebastian Scherer <basti@andrew.cmu.edu>
  *  \date    2011 October 25
  *  \details This program sends commands to a Mikrokopter rotorcraft via ROS
  */

#include "ros/ros.h"
#include "mikrokopter/Control.h"
#include <cstdlib>

int main(int argc, char **argv)
{
  ros::init(argc, argv, "mikrokopter_externctrl_client");

  ros::NodeHandle n;
  ros::Publisher pub = n.advertise<mikrokopter::Control>("/mikrokopter/req_set_control", 100);
  
  mikrokopter::Control req;

  req.digital[0] = 0;
  req.digital[1] = 0;
  req.remoteKey = 0;
  req.pitch = 0;
  req.roll = 0;
  req.yaw = 0;
  req.thrust = 40;
  req.height = 0;
  req.free = 0;
  req.frame = 7;
  req.config = 1;
  
  ros::Rate loop_rate(25);
  
  
  int maxPt = 30;
  int minPt = -30;
  int step = 2;
  int val = 30;
  
  while (ros::ok())
  {

    if (val >= maxPt)
      step = -1*step;
    if (val <= minPt)
      step = -1*step;
  
    val += step;

    std::cout << val << std::endl;
  
    req.frame++;
    req.roll = val;

    pub.publish(req);

    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}
