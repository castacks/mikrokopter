 /*!
  *  \file    externctrl_thrust_client.cpp
  *  \brief   Controls Mikrokopter thrust via ROS
  *  \author  Markus Kühn <markusk@andrew.cmu.edu>
  *  \date    2013 June 17
  *  \details This program sends thrust commands to a Mikrokopter rotorcraft via ROS while using the MikrokopterInterface
  */

#include "ros/ros.h"
#include "mikrokopter/Control.h"
#include <cstdlib>
#include <iostream>


int main(int argc, char **argv)
{
  ros::init(argc, argv, "mikrokopter_externctrl_thrust_client");
  ros::NodeHandle n;
  ros::Publisher pub = n.advertise<mikrokopter::Control>("/mikrokopter/req_set_control", 100);
  //ros::Publisher pub = n.advertise<mikrokopter::Control>("/mk_model/command", 100);
  
  
  ros::Time last_input_time;
  mikrokopter::Control req;

  req.digital[0] = 0;
  req.digital[1] = 0;
  req.remoteKey = 0;
  req.pitch = 0;
  req.roll = 0;
  req.yaw = 0;
  req.thrust = 0;
  req.height = 0;
  req.free = 0;
  req.frame = 7;
  req.config = 1;
  
  ros::Rate loop_rate(25);

std::cout << req.thrust << std::endl;
  
  //time difference between last thrust set and actual time
  int timediff;
  int temp;
  while (ros::ok())
  {

	//typecasts between int and 8bit int (treated as char) needed!!!
	ROS_INFO_STREAM("Enter a new thrust value! Last value: "<<(int)req.thrust);	
	//Read new thrust value	
	std::cin>>temp;

	if(temp <=255) 
	{
		req.thrust = (uint8_t) temp; //thrust_level[temp-1];
		last_input_time = ros::Time::now();	
	}
	else 
	{ROS_INFO("Try Again. 255 is maximum");}
	

    

    

    //std::cout << req.thrust << std::endl;
  
    req.frame++;

    pub.publish(req);

    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}
